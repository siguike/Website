<?php namespace Ske\IO;

interface Stream_Interface {
    public function setStream($stream);
    public function getStream();
}
